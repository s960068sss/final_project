import React from 'react';
import PropTypes from 'prop-types';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'
import './Setting.css';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import Main from 'components/Main.jsx';
export default class Setting extends React.Component {
    static propTypes = {
        masking: PropTypes.bool,
        group: PropTypes.string,
        description: PropTypes.string,
        temp: PropTypes.number,
        unit: PropTypes.string
    };

    constructor(props) {
        super(props);

    }

    render() {
        return(
            <Router>
                <div>
                    <div>
                        <Nav navbar>
                            <NavItem>
                                <NavLink tag={Link} to='/'>Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={Link} to='/StudyPlan'>StudyPlan</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={Link} to='/Practice'>Practice</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={Link} to='/QuestionBank'>QuestionBank</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={Link} to='/Friend'>Friend</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={Link} to='/WhiteList'>WhiteList</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={Link} to='/'>back</NavLink>
                            </NavItem>
                        </Nav>
                    </div>
                    <Route exact path="/" render={() => (
                            <Main  />
                    )}/>
                    <Route exact path="/StudyPlan" render={() => (
                            <Main  />
                    )}/>
                    <Route exact path="/Practice" render={() => (
                            <Main  />
                    )}/>
                    <Route exact path="/QuestionBank" render={() => (
                           <Main  />
                    )}/>
                    <Route exact path="/Friend" render={() => (
                            <Main  />
                    )}/>
                    <Route exact path="/WhiteList" render={() => (
                            <Main  />
                    )}/>
                    <Route exact path="/" render={() => (
                            <Main  />
                    )}/>
                </div>
            </Router>
            
            
        );
    }
}
