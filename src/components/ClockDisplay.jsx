import React from 'react';
import PropTypes from 'prop-types';

import './ClockDisplay.css';

export default class ClockDisplay extends React.Component {
    static propTypes = {
        masking: PropTypes.bool,
        group: PropTypes.string,
        description: PropTypes.string,
        temp: PropTypes.number,
        unit: PropTypes.string
    };

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div>
                <img src={`images/clock.png`} className="clock"/>
                <div className="time">am <nobr className="pm">pm</nobr></div>
            </div>
        );
    }
}
