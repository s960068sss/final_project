import React from 'react';
import PropTypes from 'prop-types';
import {
    Form,
    Input,
    Button,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';


import './Timer.css';

export default class Timer extends React.Component {
    static propTypes = {
        masking: PropTypes.bool,
        group: PropTypes.string,
        description: PropTypes.string,
        temp: PropTypes.number,
        unit: PropTypes.string,
        time: PropTypes.number
    };

    constructor(props) {
        super(props);
        this.state = {
            time_sec: 0, 
            time_min: 0,
            button_state:'start'
        };
        this.timer_stop = this.timer_stop.bind(this);
        this.timer_restart = this.timer_restart.bind(this);
        this.timer_plus = this.timer_plus.bind(this);
        this.stop = 1;
    }


    timer_plus() {
        if(this.state.time_sec!=59){
            this.setState({time_sec: this.state.time_sec + 1});
        }
        else{
            this.setState({time_sec: 0, time_min: this.state.time_min + 1});
        }
        // console.log(this.state.time)
    }
    timer_stop() {
        if (this.stop==0){
            this.stop = 1;
            this.setState({button_state:'start'});
            clearInterval(this.interval);
        }
        else{
            this.stop = 0;
            this.setState({button_state:'stop'});
            this.interval = setInterval(this.timer_plus, 100);
        }
        
    }
    timer_restart() {
        this.setState({time_sec: 0, time_min:0});
        this.setState({button_state:'start'});
        this.stop = 1;
        clearInterval(this.interval);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    render() {
        return (
            <div>
                <div className="Timer">{this.state.time_min}:{this.state.time_sec}</div>
                <button className="button" onClick={this.timer_stop}>{this.state.button_state}</button>
                <button className="button" onClick={this.timer_restart}>Restart</button>
                
            </div>
        )
    }
}

