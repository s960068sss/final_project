import React from 'react';
import PropTypes from 'prop-types';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'
import './Schedual.css';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
export default class Schedual extends React.Component {
    static propTypes = {
        masking: PropTypes.bool,
        group: PropTypes.string,
        description: PropTypes.string,
        temp: PropTypes.number,
        unit: PropTypes.string
    };

    constructor(props) {
        super(props);
        this.getInputValue = this.getInputValue.bind(this);

    }

    getInputValue(){
        // Selecting the input element and get its value 
        var inputVal = document.getElementById("schedual").value;
        
        // Displaying the value
        console.log(inputVal);
        
    }
    render() {
        return(
            
                <div>               
                    <input type="text" maxLength="512" id="schedual" className="searchField"/>
                    <input type="button" value="Get Value" onClick={this.getInputValue}/>
                </div>
                    
            
            
        );
    }
}
