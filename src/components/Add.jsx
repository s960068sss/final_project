import React from 'react';
import PropTypes from 'prop-types';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';

import Schedual from 'components/Schedual.jsx';
import Today from 'components/Today.jsx';
import './Add.css';
import Setting from 'components/Setting.jsx';

export default class Add extends React.Component {
    static propTypes = {
        masking: PropTypes.bool,
        group: PropTypes.string,
        description: PropTypes.string,
        temp: PropTypes.number,
        unit: PropTypes.string
    };

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div>
                <Router>
                    <NavItem tag={Link} to='/schedual' ><img src="images/add.png" className="add"></img></NavItem>
                    <Route exact path="/schedual" render={() => (
                        <Schedual/>
                    )}/>
                </Router>
            </div>
        );
    }
}
