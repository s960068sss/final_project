import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';

import Today from 'components/Today.jsx';
import Add from 'components/Add.jsx';
import Forecast from 'components/Forecast.jsx';
import Setting from 'components/Setting.jsx';
import Schedual from 'components/Schedual.jsx';
import './Main.css';

export default class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            unit: 'metric',
            navbarToggle: false
        };

        this.handleNavbarToggle = this.handleNavbarToggle.bind(this);
        this.handleUnitChange = this.handleUnitChange.bind(this);
    }

    render() {
        return (
            <Router>
                <div className={`main bg-faded ${this.state.group}`}>
                    <div className='container'>
                        <Navbar color="faded" light expand="md">
                            <NavbarToggler onClick={this.handleNavbarToggle}/>
                            <Collapse isOpen={this.state.navbarToggle} navbar>
                                <Nav navbar>
                                    <NavItem>
                                        <NavLink tag={Link} to='/' >home</NavLink>
                                        <NavLink tag={Link} to='/setting' ><img src="images/setting.png" className="setting"></img></NavLink>
                                        <NavLink tag={Link} to='/schedual' ><img src="images/add.png" className="add"></img></NavLink>
                                    </NavItem>
                                    
                                </Nav>
                                
                            </Collapse>
                        </Navbar>
                    </div>

                    <Route exact path="/" render={() => (
                        <Today/>
                    )}/>
                    <Route exact path="/setting" render={() => (
                        <Setting />
                    )}/>
                    <Route exact path="/schedual" render={() => (
                        <Schedual />
                    )}/>
                </div>
            </Router>
        );
    }

    handleNavbarToggle() {
        this.setState((prevState, props) => ({
            navbarToggle: !prevState.navbarToggle
        }));
    }

    handleUnitChange(unit) {
        this.setState({
            unit: unit
        });
    }
}
