import React from 'react';
import PropTypes from 'prop-types';

import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';

import Setting from 'components/Setting.jsx';
import './Alert.css';

export default class Alert extends React.Component {
    static propTypes = {
        masking: PropTypes.bool,
        group: PropTypes.string,
        description: PropTypes.string,
        temp: PropTypes.number,
        unit: PropTypes.string
    };

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div> 
                
                <img src="images/alert.png" className="alert"></img>
                
            </div>

        );
    }
}
